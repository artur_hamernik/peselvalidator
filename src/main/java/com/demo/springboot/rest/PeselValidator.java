package com.demo.springboot.rest;

import java.util.ArrayList;
import java.util.List;

public class PeselValidator {

    private boolean valid = false;

    public PeselValidator(String numberPesel) {
        validate(numberPesel);
    }

    public boolean isValid() {
        return valid;
    }

    private void validate(String numberPesel) {
        if (numberPesel.length() == 11) {
            List<Integer> list = new ArrayList<>();
            for(int i = 0; i < numberPesel.length(); i++){
                list.add(Integer.parseInt(numberPesel.substring(i, i+1)));
            }

            int check = list.get(0) + 3 * list.get(1) + 7 * list.get(2)
                    + 9 * list.get(3) + list.get(4) + 3 * list.get(5) + 7
                    * list.get(6) + 9 * list.get(7) + list.get(8) + 3
                    * list.get(9);

            int lastNumber = check % 10;
            int controlNumber = 10 - lastNumber;

            if (controlNumber == list.get(10)) {
                valid = true;
            } else {
                valid = false;
            }

        }
        else {
            valid = false;
        }
    }
}
